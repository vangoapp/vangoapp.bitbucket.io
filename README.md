# VanGO

Esse é pequeno projeto feito para fins academicos apenas com HTML/CSS/JavaScript.

Para visualizar essa aplicação utilize o link [https://vangoapp.bitbucket.io](https://vangoapp.bitbucket.io).

### Grid
Essa é uma grid de 12 colunas. E foi construido utilizando o conceito mobile-first. Composta por uma linha(`div.row`) e pelas colunas(`div.col-*`). Em telas pequenas a coluna vai expandir no tamanho total da tela. No caso de precisar dividir em colunas em dispositivos moveis também utilize a classe com suffixo **-sm**. Exemplo `.col-6-sm`.

Exemplo:
```html
<div class="container">
  <div class="row">
    <div class="col-3">
      <!-- Essa div vai ter 3/12 (ou 1/4) do container -->
    </div>
    <div class="col-3">
      <!-- Essa div vai ter 3/12 (ou 1/4) do container -->
    </div>
    <div class="col-6">
      <!-- Essa div vai ter 6/12 (ou 1/2) do container -->
    </div>
  </div>
</div>
```

### Fontes
Estamos usando a biblioteca de icones [Font Awesome](https://fontawesome.com). Para ver a lista completa de icones [clique aqui](https://fontawesome.com/icons?d=gallery)

Exemplo:

```html
<i class="fas fa-map-marker"></i>
```
